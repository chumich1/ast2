    
import sys
from namespace import Namespace    
    
class SymbolTable:    
        
    def __init__(self):    
        self.scopes = []
        self.address = 0    
        self.globalCount = 0
        self.namespace = Namespace()    
    
    def retrieveSymbol(self, name):    
        if type(name) == str:
            node = self.namespace.findWord(name)    
        else:
            node = name
        for i in reversed(list(range(len(self.scopes)))):    
            if node in self.scopes[i]:    
                return self.scopes[i][node]    
        return None    
    
    def openScope(self):    
        self.scopes.append({})    
    
    def closeScope(self):    
        if len(self.scopes) == 1:
            self.globalCount = len(self.scopes[0].keys())
        oldScope = self.scopes.pop()
        self.address -= len(oldScope)    
    
    def declaredLocally(self, name):    
        if type(name) == str:
            name = self.namespace.findWord(name)
        return name in self.scopes[-1]    
    
    # should we check list size first?    
    def enterSymbol(self, name, value, isConst=False, overrideGlobal=False):    
        node = self.namespace.addWord(name)    
        if node in self.scopes[0].keys() and not overrideGlobal:
            print("Cannot shadow global var:", name, file=sys.stderr)
            exit(1)
        self.scopes[-1][node] = (value, self.address, isConst)    
        self.address += 4

    def getAddress(self, node):
        value = self.retrieveSymbol(node)
        if value is not None:
            return value[1]

    def getValue(self, node):
        value = self.retrieveSymbol(node)
        if value is not None:
            return value[0]

    def getIsConst(self, node):
        value = self.retrieveSymbol(node)
        if value is not None:
            return value[2]

#    def __str__(self):
#        string = ''
#        for scope in self.scopes:
#            string += "scope"
#            for key in scope.keys():
#                string += scope[key]
#        return string
