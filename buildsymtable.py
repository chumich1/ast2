from symbolTable import SymbolTable
from langyacc import parseTokens, runPrint2, printParseTree
from ASTNode import *
from IRGenerator import IRGenerator
import sys, re


class Main:

    def __init__(self):
        self.symbolTable = SymbolTable()
        self.root = None
        self.irOutput = ''
        self.instructionAddressStart = 0
        self.globalsAddressStart = None
        self.heapStart = None

    def main(self):
        outputName = sys.argv[1]
        self.root = parseTokens()
        # print out raw tree
        runPrint2(self.root, "%s.p" % outputName)
        self.transformArithmetic(self.root)
        self.buildSymbolTable(self.root)

        self.printTree(self.root)
        
        # print out transformed tree
        runPrint2(self.root, "%s.a" % outputName)
        # write IR
        self.adjustSymbolAdresses()
        # get heap start address
        self.heapStart = self.globalsAddressStart + (self.symbolTable.globalCount * 4)
        fp = open("%s.ir" % outputName, 'w')
        fp.write(self.irOutput)
        fp.close()
        # write memory map
        fp = open("%s.memmap" % outputName, 'w')
        print("Memory Map", file=fp)
        print("Instruction Start:", self.instructionAddressStart, file=fp)
        print("Globals Start:", self.globalsAddressStart, file=fp)
        print("Heap Start:", self.heapStart, file=fp)
        fp.close()


    def adjustSymbolAdresses(self):
        instructionList = self.irOutput.split('\n')
        self.globalsAddressStart = len(instructionList) * 4
        instructionRegex = re.compile("(mem.*)(\d+)")
        index = 0
        while index < len(instructionList):
            match =  instructionRegex.match(instructionList[index])
            if match is not None:
                oldAddress = int(match.group(2))
                newAddress = oldAddress + (len(instructionList) * 4)
                instructionList[index] = "%s%d" %(match.group(1), newAddress)
            index += 1
        self.irOutput = '\n'.join(instructionList)
    







    def printTree(self, node, depth=0):
        print(' ' * depth, node, "ullman=%d" % node.sethiUllmanNum, "value=%s" % str(node.getValue()))
        for child in node.getChildren():
            self.printTree(child, depth + 2)

    def buildSymbolTable(self, root):
        self.irOutput = self.processNode(root)


    def processNode(self, node):
        #Init a new IRGenerator with the current symbol table
        gen = IRGenerator(self.symbolTable)

        print(self.symbolTable.scopes)
        if type(node) == BlockNode:
            self.symbolTable.openScope()
        elif type(node) == AssignNode:
            leftChild = node.getLeftmostChild()
            rightChild = leftChild.getRightSib()
            #print("Left Child: ", leftChild.getName(), " init: ", leftChild.getInit())
            print(leftChild, leftChild.getName(), leftChild.getInit())
            if leftChild.getInit():
                if self.symbolTable.declaredLocally(leftChild.getName()):
                    print("can't declare %s twice in same scope" % leftChild.getName(), file=sys.stderr)
                    exit(1)
                self.symbolTable.enterSymbol(leftChild.getName(), type(rightChild), leftChild.isConst())
            else:
                sym = self.symbolTable.retrieveSymbol(leftChild.getName())
                if sym == None:
                    print("Symbol: ", leftChild.getName(), " not initialized", file=sys.stderr)
                    exit(1)
                elif sym[2] == True:
                    print("Symbol:", leftChild.getName(), "is a constant", file=sys.stderr)
                    exit(1)

        elif type(node) == IdentifierNode:
            sym = self.symbolTable.retrieveSymbol(node.getName())
            if sym == None:
                print("undeclared symbol", node.getName(), file=sys.stderr)
                exit(1)

        #Generate the code returns string
        irOutput = gen.genCode(node)

        #If else requires special jump after the true case block to skil
        if type(node) == IfNode:
            condition = node.getLeftmostChild()
            trueBlock = condition.getRightSib()
            falseBlock = trueBlock.getRightSib()

            #irOutput += self.processNode(condition)
            trueCode = self.processNode(trueBlock)

            irOutput += gen.irIf(node, trueCode.count('\n'))
            irOutput += trueCode

            if falseBlock is not None :
                falseCode = self.processNode(falseBlock)
                irOutput += gen.ifCloseTrueBlock(node, falseCode.count('\n'))
                irOutput += falseCode

        elif type(node) == WhileNode:
            condition = node.getLeftmostChild()
            loopBlock = condition.getRightSib()

            loopCode = self.processNode(loopBlock)

            (code, condLen) = gen.irWhile(node, loopCode.count('\n'))

            irOutput += code
            irOutput += loopCode
            irOutput += gen.closeWhileBlock(node, condLen + loopCode.count('\n'))

                
        else:
            for child in node.getChildren():
                irOutput += self.processNode(child)

        if type(node) == BlockNode:
            self.symbolTable.closeScope()


        if type(node) == ReturnNode:
            irOutput += "reljump +0\n"

        return irOutput

    def calculateSethiUllmanNums(self, node):
        #if type(node) in [ShiftLNode, ShiftRNode, PlusNode, MinusNode, MultNode, DivNode, IdentifierNode, IntNode]:
        if len(node.getChildren()) == 0: # leaf node
            node.sethiUllmanNum = 1
        else: # not a leaf node
            for child in node.getChildren():
                self.calculateSethiUllmanNums(child)
            if node.leftmost_child is not None and node.leftmost_child.right_sibling is not None:
                leftChild = node.leftmost_child
                rightChild = leftChild.right_sibling
                if leftChild.sethiUllmanNum == rightChild.sethiUllmanNum:
                    node.sethiUllmanNum = leftChild.sethiUllmanNum + 1
                else:
                    node.sethiUllmanNum = max(leftChild.sethiUllmanNum, rightChild.sethiUllmanNum)

    def transformArithmetic(self, node):
        if type(node) in [ShiftLNode, ShiftRNode, PlusNode, MinusNode, MultNode, DivNode]:
            parent = node.getParent()
            node = self.makeArithmeticTree(node)

            parent.getLeftmostChild().setRightSib(node)
            node.setParent(parent)
            node.setLeftmostSib(parent.getLeftmostChild())

            self.calculateSethiUllmanNums(node)
        else:
            for child in node.getChildren():
                self.transformArithmetic(child)


    def makeArithmeticTree(self, node):
        operaterPrecedence = [[MultNode, DivNode], [PlusNode, MinusNode], [ShiftLNode, ShiftRNode]]
        xs = self.flattenTree(node)
        for n in xs:
            n.destroyFamily()
        for ops in operaterPrecedence:
            index = 0
            while index < len(xs):
                if type(xs[index]) in ops:
                    node = xs[index]
                    right = xs.pop(index + 1)
                    left = xs.pop(index - 1)
                    node.setLeftmostChild(left)
                    left.setRightSib(right)
                    right.setParent(node)
                    left.setParent(node)
                else:
                    index += 1

        return xs[0]



    def flattenTree(self, node):
        if node == None:
            return []
        left = node.getLeftmostChild()
        if left == None:
            return [node]
        xs = []
        right = left.getRightSib()
        xs += self.flattenTree(left) + [node] + self.flattenTree(right)
        node.destroyFamily()
        return xs



# Variables for printing
NodeNum = 0
NodeList = []
ConnectionList = {}

# This adds the name of the child to
# the parent dictionary for printing
def addNameOfChildToParent(parent, child):
    global ConnectionList
    if parent in ConnectionList:
        ConnectionList[parent].append(child)
    else:
        ConnectionList[parent] = [child]

def printTree2(node):
    global NodeNum, NodeList, ConnectionList
    current = node
#    print("My type is: ", node.getType(), " my mom is ", node.getParent())
    current_name = node.getType() + str(NodeNum)
    node.setPrintName(current_name)
    NodeList.append(current_name)
    NodeNum = NodeNum + 1 

    if node.getLeftmostChild() is not None:
        printTree2(node.getLeftmostChild())
#        print("left")

    if node.getRightSib() is not None:
        printTree2(node.getRightSib())
#        print("right")
    
    if node.getParent() is not None:
#        print("setting")
        parent_name = node.getParent().getPrintName()
        #NodeList.append(current_name)
        addNameOfChildToParent(parent_name, current_name)
        #NodeNum = NodeNum + 1




if __name__ == "__main__":
    main = Main()
    main.main()
