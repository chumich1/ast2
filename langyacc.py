import ply.yacc as yacc
from ASTNode import *
import argparse, sys
#def makeNode():

#def makeNode(type):

def makeNode(typeOfNode, value=None, isInit=False, isConst=False):

    if typeOfNode == 'int':
        return IntNode(value)
    elif typeOfNode == 'assign':
        return AssignNode()
    elif typeOfNode == 'id':
        return IdentifierNode(value, isInit, isConst)
    elif typeOfNode == 'block':
        return BlockNode()
    elif typeOfNode == 'start':
        return StartNode()
    elif typeOfNode == 'eq':
        return BoolEQNode()
    elif typeOfNode == 'geq':
        return BoolGEQNode()
    elif typeOfNode == 'leq':
        return BoolLEQNode()
    elif typeOfNode == 'neq':
        return BoolNEQNode()
    elif typeOfNode == 'gt':
        return BoolGTNode()
    elif typeOfNode == 'lt':
        return BoolLTNode()
    elif typeOfNode == 'if':
        return IfNode()
    elif typeOfNode == 'shiftl':
        return ShiftLNode()
    elif typeOfNode == 'shiftr':
        return ShiftRNode()    
    elif typeOfNode == 'plus':
        return PlusNode()
    elif typeOfNode == 'minus':
        return MinusNode()
    elif typeOfNode == 'mult':
        return MultNode()
    elif typeOfNode == 'div':
        return DivNode()
    elif typeOfNode == 'while':
        return WhileNode()
    elif typeOfNode == 'mod':
        return ModNode()
    elif typeOfNode == 'return':
        return ReturnNode()
    else: 
        return None



def printTree(node):
    print("My type is: ", node.getType())
    if node.getLeftmostChild() is not None:
        print("My children are: ")
        printTree(node.getLeftmostChild())
        print("/end children")

    if node.getRightSib() is not None:
        print("My siblings are:")
        printTree(node.getRightSib())
        print("/end siblings")

    print("/end node")

# Variables for printing
NodeNum = 0
NodeList = []
ConnectionList = {}

# This adds the name of the child to
# the parent dictionary for printing
def addNameOfChildToParent(parent, child):
    global ConnectionList
    if parent in ConnectionList:
        ConnectionList[parent].append(child)
    else:
        ConnectionList[parent] = [child]

def runPrint2(node, filename):
    global NodeNum, NodeList, ConnectionList
    NodeNum = 0
    NodeList = []
    ConnectionList = {}
    fp = open(filename, 'w')
    printTree2(node)
    printParseTree(fp)
    fp.close()


def printTree2(node):
    global NodeNum, NodeList, ConnectionList
    current = node
#    print("My type is: ", node.getType(), " my mom is ", node.getParent())
    current_name = node.getType() + str(NodeNum)
    node.setPrintName(current_name)
    NodeList.append(current_name)
    NodeNum = NodeNum + 1 

    if node.getLeftmostChild() is not None:
        printTree2(node.getLeftmostChild())
#        print("left")

    if node.getRightSib() is not None:
        printTree2(node.getRightSib())
#        print("right")
    
    if node.getParent() is not None:
#        print("setting")
        parent_name = node.getParent().getPrintName()
        #NodeList.append(current_name)
        addNameOfChildToParent(parent_name, current_name)
        #NodeNum = NodeNum + 1

def printParseTree(fp):
    for n in NodeList:
        print(n + " Node " + n, file=fp)
    print("", file=fp)
    s = ""
    for d in ConnectionList:
        s = ''
        ConnectionList[d].reverse()
        for x in ConnectionList[d]:            
            s = s + " " + x
        print(d + " " + s, file=fp)

# Get the token map from the lexer.  This is required.
from tokenizer import tokens

precedence = (
    ('left', 'PLUS', 'MINUS'),
    ('left', 'MULT', 'DIV'),
)


def p_start(p):
    """
    start : block
    """
    start = makeNode('start')
    makeFamily(start, [p[1]])
    p[0] = start

def p_block(p):
    """
    block : stmts
    """   
    block = makeNode('block')
    makeFamily(block, [p[1]])
    p[0] = block

def p_statements(p):
    """
    stmts : 
    stmts : stmt stmts
    """


    if len(p) != 1:

        
        #makeFamily(statements, [p[1]])
        #if p[2] is not None:
        #    statements.makeSiblings(p[2])
        #p[0] = statements
        if p[1] is not None and p[2] is not None:
            p[1].makeSiblings(p[2])
        p[0] = p[1]
        #if p[2] is not None:

    

def p_statement(p):
    """
    stmt : decls SEMI
    stmt : expr SEMI
    stmt : if
    stmt : RETURN expr SEMI
    stmt : RETURN SEMI
    stmt : LCURLY block RCURLY
    stmt : while

    """
#    stmt : RETURN expr SEMI
    if p[1] == 'return' and len(p) == 3:
        rNode = makeNode('return')
        print("GOT TO PRINT;")
        p[0] = rNode
    elif len(p) == 3:
        p[0] = p[1]
    elif p[1] == 'return':
        rNode = makeNode('return')
        makeFamily(rNode, [p[2]])
        p[0] = rNode
    elif p[1] == '{':
        p[0] = p[2]
    else:
        p[0] = p[1]

def p_if(p):
    """
    if : IF LPAREN expr RPAREN LCURLY block RCURLY
    if : IF LPAREN expr RPAREN LCURLY block RCURLY ELSE LCURLY block RCURLY
    """

    if len(p) == 12:
        nodeIF = makeNode('if')

        makeFamily(nodeIF,[p[3], p[6], p[10]])

        p[0] = nodeIF


    else: # len(p) == 8:
        nodeIF = makeNode('if')
        makeFamily(nodeIF,[p[3],p[6]])
        p[0] = nodeIF
    #el

def p_while(p):
    """
    while : WHILE LPAREN expr RPAREN LCURLY block RCURLY
    """
   
    nodeWhile = makeNode('while')
    makeFamily(nodeWhile, [p[3], p[6]])
    p[0] = nodeWhile

def p_expr(p):

    """
    expr : expr op expr
    expr : val
    expr : LPAREN expr RPAREN
    """


    oplist = ['bool_eq', 'bool_geq', 'bool_leq', 'bool_neq', 'bool_gt', 'bool_lt']
    if len(p) == 2: 
        p[0] = p[1]
    elif p[1] == "(":
        p[0] = p[2]
    elif len(p) == 4:
        makeFamily(p[2],[p[1],p[3]])
        p[0] = p[2]
 
def p_op(p):
    """
    op : MINUS
    op : PLUS
    op : MULT
    op : SHIFTR
    op : SHIFTL
    op : DIV
    op : EQ
    op : GEQ
    op : LEQ
    op : NEQ
    op : GT
    op : LT
    op : MOD
    """

    if p[1] == '+':
        p[0] = makeNode('plus')
    elif p[1] == '-':
        p[0] = makeNode('minus')
    elif p[1] == '*':
        p[0] = makeNode('mult')
    elif p[1] == '>>':
        p[0] = makeNode('shiftr')
    elif p[1] == '<<':
        p[0] = makeNode('shiftl')
    elif p[1] == '/':
        p[0] = makeNode('div')
    elif p[1] == '==':
        p[0] = makeNode('eq')
    elif p[1] == '>=':
        p[0] = makeNode('geq')
    elif p[1] == '<=':
        p[0] = makeNode('leq')
    elif p[1] == '!=':
        p[0] = makeNode('neq')
    elif p[1] == '>':
        p[0] = makeNode('gt')
    elif p[1] == '<':
        p[0] = makeNode('lt')
    elif p[1] == '%':
        p[0] = makeNode('mod')


def p_type(p):
    """
    type : INT
    """


def p_decls(p):
    """
    decls : decls COMMA ID ASSIGN expr
    decls : decls COMMA ID
    decls : decl

    """

    if len(p) == 2: #decl
        p[0] = p[1]
    elif len(p) == 4: #decls COMMA ID
        assign = makeNode('assign')
        noneNode = makeNode('int', 0)
        iden = makeNode('id', p[3], True)
        makeFamily(assign, [iden, noneNode])
        p[1].makeSiblings(assign)
        p[0] = p[1]
    elif len(p) == 6:
        assign = makeNode('assign')
        iden = makeNode('id', p[3], True)
        makeFamily(assign, [iden, p[5]])
        p[1].makeSiblings(assign)
        p[0] = p[1]
         #FIX THIS SOON


def p_decl(p):
    """
    decl : type ID ASSIGN expr
    decl : CONST type ID ASSIGN expr
    decl : type ID
    decl : CONST type ID
    decl : ID ASSIGN expr

    """

    if p[2] == '=': #ID ASSIGN expr
        assign = makeNode("assign", p[2])
        iden = makeNode("id", p[1])
        makeFamily(assign, [iden, p[3]])
        p[0] = assign
    elif len(p) == 5 and p[3] == '=': # type ID ASSIGN expr
        assign = makeNode("assign", p[3])
        iden = makeNode("id", p[2], True)
        makeFamily(assign, [iden, p[4]])
        p[0] = assign
    elif len(p) == 6 and p[4] == '=': # CONST type ID ASSIGN expr
        assign = makeNode("assign", p[4])
        iden = makeNode("id", p[3], True, True)
        makeFamily(assign, [iden, p[5]])
        p[0] = assign
    elif len(p) == 3: # type ID
        assign = makeNode('assign')
        noneNode = makeNode('int', 0)
        iden = makeNode('id', p[2], True)
        makeFamily(assign, [iden, noneNode])
        p[0] = assign
        
    elif len(p) == 4 and p[1] == 'const': #CONST type ID
        assign = makeNode('assign')
        iden = makeNode("id", p[3], True, True)
        noneNode = makeNode('int', 0)
        makeFamily(assign, [iden, noneNode])
        p[0] = assign


def p_val(p):
    """
    val : ID
    val : NUM
    val : MINUS ID
    val : MINUS NUM
    """
    if len(p) == 2 and type(p[1]) == int:
        intNode = makeNode("int", p[1])
        p[0] = intNode
    elif len(p) == 2 and type(p[1]) == str:
        idNode = makeNode("id", p[1])
        p[0] = idNode
    elif type(p[2]) == int:
        numNode = makeNode("int", -p[2])
        p[0] = numNode
    else:
        zeroNode = makeNode("int", 0)
        minusNode = makeNode('minus')
        idNode = makeNode("id", p[2])
        makeFamily(minusNode, [zeroNode, idNode])
        p[0] = minusNode
        

def p_error(p):
    print("Syntax error at '%s'" % p.value, file=sys.stderr)
    exit(1)

def parseTokens():    
    yacc.yacc()

    #ap = argparse.ArgumentParser(description = "Team 404's parser")
    #ap.add_argument("file", help = "The file to parse")
    #args = ap.parse_args()
#    with open(file_name, 'r') as f:
#        read_data = f.read()
#        parent = yacc.parse(read_data)
#        runPrint2(parent)
#        printParseTree()
#    f.closed
    read_data = ''
    for line in sys.stdin:
        read_data += line
    parent = yacc.parse(read_data)    
    return parent

#parseTokens()

#parent = yacc.parse("""
#    // language example 0
#    const int a = 1;
#    int x = 1<<a;
#    int y, z = 3;
#
#    y = z - x;
#    if ( y <= 0 ) {
#       z = (x+2) + z*z ;
#    } else {
#       z = z / y;
#    }
#    // another comment
#    return z;
#""")
#printTree2(parent)
#printParseTree()
