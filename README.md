# Compiler Design | Hellman | Spring 2014

## Original Team:
### Maria Deslis | Eric Young | David Alexander | Jason Steinberg | Andrew Chuchmich

## Updated Team (0x45)
### Andrew Chumich | Paulo Iza | Austin Diviness | Chris Copper

##Repo Description:
This repo contains our work for the AST section of this course.
Includes comment feature!


0x45 changes:
work up to IR

##How To Run:

$ ./compile outputName < example.k 

To generate PNGs:

$ ./makegraph outputName.(a|p)


NOTE: Memory map can be found in outputName.memmap