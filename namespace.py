
class NamespaceNode:

    def __init__(self, data, parent):
        self.data = data
        self.parent = parent
        self.children = []
        self.beginsWord = False

    def hasChild(self, ch):
        '''if node has a child with ch for its data, return true, else false'''
        for child in self.children:
            if child.data == ch:
                return True
        return False

    def getChild(self, ch):
        '''returns child whose data matches ch, else returns None'''
        for child in self.children:
            if child.data == ch:
                return child
        return None

    def addChild(self, ch):
        '''adds child node to node, with data set to ch'''
        child = NamespaceNode(ch, self)
        self.children.append(child)
        
    

class Namespace:

    def __init__(self):
        self.root = NamespaceNode('', None)

    def addWord(self, word):
        '''adds word to namespace, returns node that starts word'''
        index = len(word) - 1
        node = self.root
        while index >= 0:
            currentChar = word[index]
            if node.hasChild(currentChar):
                node = node.getChild(currentChar)
            else:
                node.addChild(currentChar)
                node = node.getChild(currentChar)
            index -= 1
        node.beginsWord = True
        return node

    def getWord(self, node):
        '''returns word that node points to'''
        word = ''
        while node != self.root:
            word += node.data
            node = node.parent
        return word

    def findWord(self, word):
        '''returns node that starts word, if word in namespace. if it isn't, returns None'''
        word = word[::-1]
        node = self.root
        for ch in word:
            if node.hasChild(ch):
                node = node.getChild(ch)
            else:
                return None
        return node


            
        
    
    
