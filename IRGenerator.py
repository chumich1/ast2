from symbolTable import SymbolTable
from langyacc import parseTokens, runPrint2, printParseTree
from ASTNode import *

class IRGenerator:

    def __init__(self, symTable):
        self.symTable = symTable
        self.map = {BoolEQNode : "==",
                    BoolGTNode : ">",
                    BoolLTNode : "<",
                    BoolGEQNode : ">=",
                    BoolLEQNode : "<=",
                    BoolNEQNode : "!=",
                    ModNode : "%",
                    ShiftLNode : "<<",
                    ShiftRNode : ">>",
                    PlusNode : "+",
                    MinusNode : "-",
                    MultNode : "*",
                    DivNode : "/"}

    def genCode(self, node):
        print("genCode", self.symTable)
        nType = type(node)

        if nType == AssignNode :
            return self.irAssign(node)        
        else:
            return ""


    #ir Generation for an Assign node
    def irAssign(self, node):
        idNode = node.getLeftmostChild()
        valNode =  node.getLeftmostChild().getRightSib()
        varName = idNode.getName()

        #Int const x = 5
        if(type(valNode) == IntNode):
            command =   "immld R9, %d\n" % (valNode.getValue())
            command +=  "memst R9, %d\n" % (self.symTable.getAddress(varName))
        #Var to var x = y
        elif(type(valNode) == IdentifierNode):
            command =   "memld R9, %d\n" % (self.symTable.getAddress(valNode.getName()))
            command =   "memst R9, %d\n" % (self.symTable.getAddress(varName))
        #Expr x = Expr
        else:
            command =   self.irCalc(valNode)
            command +=  "memst R9, %d\n" % (self.symTable.getAddress(varName))

        return command

    #irGeneration for an If
    def irIf(self, node, count):
        #Get the nodes for the 3 parts of the if
        condition = node.getLeftmostChild()
        trueBlock = condition.getRightSib()
        falseBlock = trueBlock.getRightSib()

        #Block to jump to if there is no if and the condition fails
        nextBlock = node.getRightSib()

        #Test condition
        command = self.irCalc(condition)

        #location to jump to if it fails (either to the else or past the else
        command += "relbfalse +%d, R0\n" % ((count + 2) * 4)


        return command

    #Jump statement to skip over the false block if it exists
    def ifCloseTrueBlock(self,node, count):
        nextBlock = node.getRightSib()

        falseBlock = node.getLeftmostChild().getRightSib().getRightSib()
        if(falseBlock is not None):
            command = "reljump +%d\n" % ((count + 1) * 4)
        else:
            command = ""

        return command

    #
    def irWhile(self, node,count):

        nextBlock = node.getRightSib()
        condition = node.getLeftmostChild()

        #Test condition
        command = self.irCalc(condition)
        condLen = command.count('\n')
        command += "relbfalse +%d, R0\n" % ((count + 2) * 4)

        return (command, condLen)

    #jump to the start of the loop
    def closeWhileBlock(self, node,count):

        command = "reljump -%d\n" % ((count + 1) * 4)

        return command

    def allocateVirtualRegs(num, virtualRegList):
        count = int(virtualRegList[-1][1:])
        vRegList = []
        for i in range(num):
            ident = 'v'+str(count+i+1)
            vRegList.append(ident)
            self.symTable.enterSymbol(ident, None, False, True)


        return vRegList


    def calcTree(self, rootNode, workRegs, regList):
        if rootNode.sethiUllmanNum > len(regList):
            regList += self.allocateVirtualRegs(rootNode.sethiUllmanNum - len(regList), filter(lambda x: x[0] == 'v', regList))
        u = regList[0]

        if type(rootNode) == IdentifierNode:
            #load workReg rootNode.getName()
            self.command += "memld %s, %d\n" % (u, self.symTable.getAddress(rootNode.getName()))
            return u
        elif type(rootNode) == IntNode:
            #load workReg rootNode.getName()
            self.command += "immld %s, %d\n" % (u,rootNode.getValue())
            return u

        if rootNode.getLeftmostChild().sethiUllmanNum > rootNode.getLeftmostChild().getRightSib().sethiUllmanNum:
            s = self.calcTree(rootNode.getLeftmostChild(), workRegs, regList[:])
            t = self.calcTree(rootNode.getLeftmostChild().getRightSib(), workRegs, regList[1:])
            if s[0] == 'v':
                #load workReg[0] @s
                self.command += "memld %s, %d\n" % (workRegs[0], self.symTable.getAddress(s))
                s = workRegs[0]
            if t[0] =='v':
                #load workRegs[1] @t
                self.command += "memld %s, %d\n" % (workRegs[1], self.symTable.getAddress(t))
                t = workRegs[1]
            u = regList[0]
            if u[0] =='v':
                #op workRegs[0] s t
                self.command += "%s %s, %s, %s\n" % (self.map[type(rootNode)] , workRegs[0], s ,t)
                u = workRegs[0]
            else:
                #op u s t
                self.command += "%s %s, %s, %s\n" % (self.map[type(rootNode)] , u, s ,t)
            return u
        else:
            t = self.calcTree(rootNode.getLeftmostChild().getRightSib(), workRegs, regList[:])
            s = self.calcTree(rootNode.getLeftmostChild(), workRegs, regList[1:])
            if s[0] == 'v':
                #load workReg[0] @s
                self.command += "memld %s, %d\n" % (workRegs[0], self.symTable.getAddress(s))
                s = workRegs[0]
            if t[0] =='v':
                #load workRegs[1] @t
                self.command += "memld %s, %d\n" % (workRegs[1], self.symTable.getAddress(t))
                t = workRegs[1]
            u = regList[0]
            if u[0] =='v':
                #op workRegs[0] s t
                self.command += "%s %s, %s, %s\n" % (self.map[type(rootNode)] , workRegs[0], s ,t)
                u = workRegs[0]
            else:
                #op u s t
                self.command += "%s %s, %s, %s\n" % (self.map[type(rootNode)] , u, s ,t)
            return u

    #ir  for a arithmetic tree
    def irCalc(self, node):
        regList = ["R0", "R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8"]
        workRegs = ['R9', 'R10', 'R11']
        self.command = ""
        self.symTable.openScope()
        self.calcTree(node, workRegs, regList)
        self.symTable.closeScope()
        #command = "calc RD, %s\n" % (node.getName())
        #command = "calc RD, %s\n" % (str(type(node)) + str(node.irId))
        return self.command
